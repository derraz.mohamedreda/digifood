# -*- coding: utf-8 -*-
"""
Created on Mon Jun  8 11:54:49 2020

@author: DIGIFAB
"""
import psycopg2 
import pandas as pd
from sqlalchemy import create_engine

'''le but de ce projet est de nettoyer les donnees d'un fichier csv
    afin de les rendre exploitables. Le sujet traité est les denrées
    alimentaires et leurs valeurs nutritionnelles afin d'obtenir des 
    informations que l'on va utiliser par la suite afin d'avoir
    une dataframe de réference pour un projet sur l'alimentation pour
    celà la premiere étape et d'importer le fichier csv non pas en entier
    mais par morceaux afin d'eviter les bugs car le fichier etant très
    volumineux celà reste une bonne solution.Dans un deuxieme temps nous 
    supprimerons les colonnes que nous jugerons inutiles ainsi que les
    valeurs inconnues dites 'unknown' à la suite du nettoyage nous 
    allons créer un dictionnaire dans lequel nous allons stocker les 
    données propres.Ensuite une importation dans une 
    table postgres sera effectuée'''


engine = create_engine('postgresql://postgres:digifab@192.168.1.79:5432/digifab')
con = engine.connect()


FICHIER_DONNEES = "openfoodfacts.csv"

df = pd.read_csv(FICHIER_DONNEES, sep = "\t",nrows=10000,
                 low_memory = False)

df.drop(['url', 'created_t', 'created_datetime',
  'last_modified_datetime','generic_name', 'purchase_places', 'stores',
  'image_small_url','image_ingredients_url', 'image_ingredients_small_url',
  'image_nutrition_url', 'image_nutrition_small_url',
  '-butyric-acid_100g', '-caproic-acid_100g',
  '-caprylic-acid_100g', '-capric-acid_100g', '-lauric-acid_100g',
  '-myristic-acid_100g', '-palmitic-acid_100g', '-stearic-acid_100g',
  '-arachidic-acid_100g', '-behenic-acid_100g', '-lignoceric-acid_100g',
  '-cerotic-acid_100g', '-montanic-acid_100g', '-melissic-acid_100g',
  '-alpha-linolenic-acid_100g', '-eicosapentaenoic-acid_100g',
  '-docosahexaenoic-acid_100g', '-linoleic-acid_100g',
  '-arachidonic-acid_100g', '-gamma-linolenic-acid_100g',
  '-dihomo-gamma-linolenic-acid_100g', '-oleic-acid_100g',
  '-elaidic-acid_100g', '-gondoic-acid_100g', '-mead-acid_100g',
  '-erucic-acid_100g', '-nervonic-acid_100g', '-sucrose_100g',
  '-glucose_100g', '-fructose_100g', '-lactose_100g', '-maltose_100g',
  '-maltodextrins_100g', '-soluble-fiber_100g', '-insoluble-fiber_100g'],
                            axis = 1, inplace = True)


def rangement_produit(dataframe):
    """remplace les unknown dans pnns_groups_1 et pnns_groups_2 par
       les éléments de la colonne catégories"""
       
    dataframe.loc[df['pnns_groups_1'] ==
                  'unknown','pnns_groups_1'] = dataframe['categories']
    dataframe.loc[df['pnns_groups_2'] ==
                  'unknown','pnns_groups_2'] = dataframe['categories']
    return dataframe


def recherchecategories():
    """création d'un dictionnaire foodtypes que l'on va remplir
       à partir des données propres des catégories"""
       
    foodTypes={}
    for categorie in categories:
        produits = donneesPropres.loc[donneesPropres['pnns_groups_1'] 
                                      == categorie,['product_name']]
        foodTypes[categorie] = []
        for prod in produits.values:
            foodTypes[categorie].append(str(prod[0]))
    return foodTypes

### Traitement de catégories et éxécution de la def rangement

data = rangement_produit(df)
dataf = data['pnns_groups_1'].value_counts()
categories = dataf[dataf>5].index
donneesPropres = rangement_produit(df)

### Résultat des catégories et du nombre d'élément associés

foodTypes = recherchecategories()
z = 0


df.to_sql('aliment', con, if_exists='replace')







