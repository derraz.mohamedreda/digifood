# -*- coding: utf-8 -*-
"""
MODÉLISATION DES STRUCTURES D'INFORMATIONS
Ce module utilise l'ORM peewee pour modéliser sous forme d'objet les
informations et leurs relations.

I - L'utilisateur :
L'utilisateur est une série d'informations, d'identifiants, de
caractéristiques liées à la nutrition ainsi que de métainformations.

L'utilisateur comme objet a pour but d'avoir les méthodes suivantes :
    - La capacité de charger et de sauvegarder plusieurs historiques,
    ex : un historique de repas.
    - A partir d'autres structures l'utilisateur doit pouvoir calculer un
    profil alimentaire personnalisé.
    - Il doit pouvoir charger un panier d'aliments et à partir de ce panier
     d'aliments obtenir une critique, des conseils ou des recettes.

L'utilisateur comme objet sera capable de charger en mémoire certaines
informations issue d'autres objets après les avoir transformés.

II - Historique des repas :
Nous définissons un repas comme étant une date, suivie d'un ensemble
d'ingrédients lié à une quantité, cette structure permet de reconstituer
les informations alimentaires et d'être chronologiquement structurées.

Elle est stockée en json, et possède une ID unique.
Cet objet sera toujours lié à un utilisateur qui possède une foreign key
pointant vers elle.

# TODO list :
 - Etape 1
    - Modéliser l'utilisateur, l'historique de repas -- FAIT 16/6
    - Modéliser le repas en local                                      --Fait
    - Méthode de génération de données aléatoires                      --Fait
        - 17 utilisateurs (promo)
        - 30 dates sur un mois 1/jours
        - 5 ingrédients/quantité par dates
    - Méthode de transfert Dataframe > JSON > Postgres et inversement  --Fait
    - Transférer les méthodes de rendu graphique
 - Etape 2
    - Méthode de personnalisation du profil nutrionnel idéal :
        - Adaptation en fonction des caractéristiques (femme/homme, etc.).
            [Collab avec groupe Webscraping]
        - Adaptation en fonction des objectifs.
        - Adaptation en fonction des Ressenti
            [Collab avec groupe Ressenti]
 - Etape 3
    - Standardisation du format d'information en ligne avec les autres
      groupes

Boss final : modéliser un utilisateur capable de remplir le cahier des
charges de l'application à travers les informations recoupées de tous les
groupes
"""
# Imports

# Bibliothèque standard
import datetime as date

# Bibliothèque externe
import pandas as pd
from peewee import Model
from peewee import (AutoField, DateField, TextField, IntegerField,
                    CharField, ForeignKeyField)
from playhouse.postgres_ext import PostgresqlExtDatabase, ArrayField
from flask_login import UserMixin

# Bibliothèque locale
import generation
import reconutritionv2 as reco

# Connecteur postgres
db = PostgresqlExtDatabase(host="192.168.1.79",
                            database="digifab",
                            user="postgres",
                            password="digifab",
                            port=5432)


class BaseModel(Model):

    class Meta:
        database = db


class Utilisateur(BaseModel, UserMixin):
    """Ceci est la modélisation d'un utilisateur au sein de notre code, il
    possède un ID unique autoincrémenté côté base de donnée, des
    caractéristiques : un mail et un pseudo, ainsi que des caractéristiques
    définissant l'utilisateur tels que son genre, sa taille, son poids, etc.
    Il finit avec des métainformations telles que la date de modification et
    la date de création.
    Un ForeignKeyField() correspond à un repas.
    """
    # Identifiant
    id_utilisateur = AutoField()
    mail = TextField(unique=True)
    pseudo = TextField(unique=True)

    # Caracteristiques Physiques
    genre = TextField(null=True)
    taille = IntegerField(null=True)
    poids = IntegerField(null=True)
    date_naissance = DateField(null=True)

    # Information Profil Alimentaire
    activite_phys = TextField(null=True)
    tag_regime = ArrayField(CharField, null=True)
    profil_perso = TextField(null=True)

    # Date de création du compte
    date_crea = DateField()

    # Information locale
    # Pointeur vers la DataFrame de l'historique des repas.
    df_repas = pd.DataFrame()

    # Fonction de test

    def _test_SUCCESS(self):
        """Fonction de test, confirme l'identité d'un utilisateur
        """
        print("SUCCESS, je suis : ", self.pseudo)
        
    def _test_profil_perso(self) :
        """Fourni les apports de référence en énergie et macro-nutriments
        au profil de l'utilisateur
        """
        self.profil_perso = {
            "lipides" : 70,
            "glucides" : 240,
            "fibres" : 27}

    # Fonction Wrapper de reconutritionv2

    def valeur_repas(self, date_repas, valeur_nutri):
        """fonction wrapper de la fonction valeur_repas du module
        reconutritionv2 Retourne la valeur nutritionel d'un repas sous forme
        de dictionnaire
        """
        return reco.valeur_repas(self.df_repas[self.df_repas["date"]
                                               == date_repas], valeur_nutri)

    def difference_carence(self, date_debut, date_fin):
        """fonction wrapper de la fonction difference_carence du module
        reconutritionv2, Retourne les valeurs numérique des carances sous
        forme de dataframe."""
        return reco.difference_carence(self, date_debut, date_fin)

    def bilan_nutritionnel(self, date_debut, date_fin):
        """fonction wrapper de la fonction bilan_nutritionnel du module
        reconutritionv2 retourne le bilan nutrtionnel d'une période sous
        forme de dictionnaire
        """
        return reco.bilan_nutritionnel(self.df_repas,
                                       reco.valeur_nutritionnelle,
                                       date_debut, date_fin)

    def graphique(self, mode, date_debut=None, date_fin=None, date_repas=None,
                  fichier="graph_repas.png"):
        """Fonction wrapper de la fonction rendu_graphique de reconutritionv2
        transfère le self de l'utilisateur actif.
        """

        reco.rendu_graphique(self, mode, date_debut, date_fin,
                             date_repas, fichier=fichier)

    # Fonction de gestion de la variable instancié df_repas.

    def sauvegarder_repas(self, repas, date_repas):
        """Cette fonction ajoute un repas dans l'historique de repas
        correspondant à l'utilisateur.
        Un repas est une dataframe pandas possèdant trois colonnes :
            date, ingredient, quantité.
        cette dataframe est passé en JSON pour être stocké.
        """
        json_repas = repas.to_json()

        HistoriqueRepas.create(id_utilisateur=self.id_utilisateur,
                               date_repas=date_repas,
                               json_repas=json_repas)

    def chargement_repas(self, date_repas=None,
                         date_debut=None, date_fin=None):
        """Cette fonction charge des lignes de repas dans la dataframe de
        l'objet utilisateur en fonction des paramètres.
        Si une date_repas est renseigné, un seul repas sera rajouter.
        Si date_debut et date_fin sont renseigné alors un ensemble de repas
        entre ces deux dates sont ajouté à la dataframe.
        """
        # Alias de mon id
        mon_id = self.id_utilisateur

        # Si un repas unique est rechercher récupère le json_repas
        # selon un date precise :
        if date_repas:
            query = (HistoriqueRepas.select(HistoriqueRepas.json_repas)
                     .where((mon_id == HistoriqueRepas.id_utilisateur)
                            & (date_repas == HistoriqueRepas.date_repas))
                     .dicts())

        # recupere le json selon une date de départ et une date de fin
        else:
            query = (HistoriqueRepas.select(HistoriqueRepas.json_repas)
                     .where((mon_id == HistoriqueRepas.id_utilisateur)
                            & (date_debut <= HistoriqueRepas.date_repas)
                            & (HistoriqueRepas.date_repas <= date_fin))
                     .dicts())

        # Parcours des resultat de ma query
        reconstitution = []
        for resultat in query:
            # Recuperation de mon Json suivi de
            # concaténation avec la df originale
            reconstitution += [pd.read_json(resultat["json_repas"])]
        # reconstruit tout les repas en une seule dataframe
        hist_repas = pd.concat(reconstitution)
        self.df_repas = pd.concat([self.df_repas, hist_repas])

        def vide_df_repas(self):
            """Ajoute un repas à la dataframe en local df_repas
            """
            self.df_repas = pd.DataFrame()
            
        def reco_perso(self) : 
            """Cette fonction permet la personalisation d'un apport
            nutritionel journalier en fonction du poids, de l'age,
            de la taille, de l'activité de la personne.
            """
            # TODO 
            # proteine = {"age" : {18 : 1.2,
            #              60 : 1.0,
            #              120 : 0.8}}
    
            
            actphysique = {"aucune" : 1.4, 
                           "peu" : 1.6, 
                           "modere" : 1.7, 
                           "régulier" : 1.8, 
                           "intense" : 2}
            
            act = actphysique[self.activite_phys]
            taille = self.taille/100
                
            if self.genre == "femme":
                # formule
                kcalj = (((9.740 * self.poids) + (172.9 * taille)
                          - (4.737 * self.age) + 667.051) * act)
            else:
                # formule
                kcalj = (((13.707 * self.poids) + (492.3 * taille)
                          - (6.673 * self.age) + 77.607) * act)
        
            
            macro = {"sport loisir": {"glucides (en %)" : 55,
                                      "lipîdes (en %)" : 30,
                                      "proteines (en %)" : 15},
                     "sport de force" : {"glucides (en %)" : 55,
                                         "lipîdes (en %)" : 22,
                                         "proteines (en %)" : 25},
                     "sport d'endurance" : {"glucides (en %)" : 65,
                                            "lipîdes (en %)" : 20,
                                            "proteines (en %)" : 15}}
            
    
            return macro, kcalj



class HistoriqueRepas(BaseModel):
    """ Ce model est la structure de stockage d'un historique de repas,
    il existe sous forme d'une date suivie d'une structure contenant
    l'information repas.
    """
    id_repas = AutoField()
    id_utilisateur = ForeignKeyField(Utilisateur)
    date_repas = DateField()
    json_repas = TextField()


def _test_insert_promo():
    """Pour chaque membre de la promo crée un compte avec une fausse adresse
    mail"""
    for mail, pseudo in generation.PROMO:
        Utilisateur.create(mail=mail, pseudo=pseudo,
                           date_crea=date.datetime.now())


def chargement_utilisateur(pseudo=None, id_utilisateur=None):
    """Retourne un utilisateur par son pseudo ou son id, retourne None si
    le chargement a échoué.
    """
    if pseudo:
        for utilisateur in (Utilisateur.select()
                                       .where(Utilisateur.pseudo == pseudo)):
            return utilisateur

    if id_utilisateur:
        for utilisateur in (Utilisateur.select()
                                       .where(Utilisateur.id_utilisateur ==
                                              id_utilisateur)):
            return utilisateur

    return None


# Deployment
if __name__ == '__main__':
    # Crée les tables dans la BDD
    creation_table = False
    # Crée 20 profils utilisateur vide dans la BDD correspondant à la promo
    insertion_utilisateur = False
    # Crée 365 repas reparti aléatoirement dans l'année 2020
    insertion_hist_repas = False
    # Test les fonction de sauvegarde/chargement de JSON
    test_historique_repas = False
    # Test la capacité à charger un utilisateur comme objet
    test_utilisateur = False

    # penser à créer la base de données sur postgreSQL
    if creation_table:
        # pour créer la table
        db.drop_tables([Utilisateur, HistoriqueRepas])
        db.create_tables([Utilisateur, HistoriqueRepas])

    # Insert les utilisateurs issue de la structure dans génération
    if insertion_utilisateur:
        # 20 pseudo/mails sans autre info.
        _test_insert_promo()
    # Insert dans historiquerepas environ 7300 repas à des dates aléatoire
    # Entre deux dates.
    if insertion_hist_repas:
        # Obtient toutes les id_utilisateur
        query = Utilisateur.select(Utilisateur.id_utilisateur)
        date_1 = date.datetime(2020, 1, 1)
        # Pour tout les utilisateurs
        for id_utilisateur in query:
            # Prepare un gestionaire de transaction
            with db.atomic():
                # Genere un dictionaire d'historique de repas
                hist_repas = generation._test_hist_repas(date_1,
                                                         id_utilisateur)
                # insert les historique de l'utilisateur
                HistoriqueRepas.insert_many(hist_repas).execute()

    # Verfie la création et l'accès à l'historique de repas, pov BDD.
    if test_historique_repas:
        date_1 = date.datetime(2020, 1, 1)
        date_2 = date.datetime(2020, 12, 31)
        query = (HistoriqueRepas.select(HistoriqueRepas.json_repas)
                                .where(HistoriqueRepas.id_utilisateur == 2)
                                .dicts())

        df = pd.DataFrame()
        for resultat in query:
            df = pd.concat([df, pd.read_json(resultat["json_repas"])])

    # Environement de test des méthodes de l'utilisateur
    if test_utilisateur:
        # Charge Lea comme utilisateur test
        for utilisateur in (Utilisateur.select()
                                       .where(Utilisateur.pseudo == "Lea")):
            Lea = utilisateur

        # Vérifie que Léa est chargé
        Lea._test_SUCCESS()
        # Vérifie que la variable self.df_repas est bien initialisé vide
        print(Lea.df_repas)

        # Prepare deux date pour couvrir l'année 2020
        date_1 = date.datetime(2020, 1, 1)
        date_2 = date.datetime(2020, 12, 31)

        # # charge une serie de repas
        Lea.chargement_repas(date_debut=date_1, date_fin=date_2)

        # print(Lea.df_repas)
