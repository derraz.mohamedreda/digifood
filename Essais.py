#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jun 18 11:29:18 2020

Ce code permets la sauvegarde d'une table en fichier csv

@author: Reda
"""

import os.path
import datetime
import time
import psycopg2
import pandas as pd
from pathlib import Path
import schedule


      

""" 1- Se connecter au serveur

    2- Ressortir les informations de la table aliment
    - Importer la table aliment
    - Selectionner la ligne qui nous interesse suivant le nom de l'aliment 
        choisi
    - Selectionner les cellules qui nous interesse dans la ligne

    3- Ressortir les informations de la table ressenti
    - Importer la table ressenti
    - Importer les 3 données négatives les + importantes
    - Importer les 3 données positives les + importantes
    - Les transformer en % si pas déjà le cas
    """

"""Si j'ai besoin de 2 infos sur 4, je prends les 4 en requete SQL et 
je trie après"""
""" Pour les pourcentages, je récupère les valeurs voulues brutes et je 
calcule le % après"""


# Connexion à la base donnée
con = psycopg2.connect(host="localhost",
                   database="alimentation",
                   user="postgres",
                   password="digifab",
                   port=5432)

colonnes =['kcal', 'proteines', 'fibre', 'sucre', 'nutriscore']




var_ali = 'Riz au lait'
quantité = 100

var_quant= quantité/100


cur = con.cursor()


sql = """select product_name as aliment, avg("energy-kcal_100g") as kcal, 
avg(proteins_100g) as proteines, avg(fiber_100g) as fibre, avg(sugars_100g) as 
sucre, avg("nutrition-score-fr_100g") as nutriscore from aliment group by 
product_name"""
cur.execute(sql)

df = pd.read_sql_query(sql,con)

val_nut=df[(df['aliment']==var_ali)]

ids=val_nut[(val_nut['aliment']==var_ali)].index[0]
for colonne in colonnes :
    val_nut.loc[ids,colonne]=val_nut.loc[ids,colonne]*var_quant
        
    


print ("Vous avez choisi '"+var_ali+"' pour '100g' :")
print ("Valeurs nutritionnelles")
print (val_nut)