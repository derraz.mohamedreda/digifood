# -*- coding: utf-8 -*-
"""
Fonction de génération de données lié a

@author: Equipe reco nutri
"""

import numpy as np
import datetime as date
import pandas as pd

# from modelisation import Utilisateur

PROMO = [["lea@gmail.com", "Lea"],
         ["Leeroy@gmail.com", "Leeroy"],
         ["Joseph@gmail.com", "Joseph"],
         ["Florian@gmail.com", "Florian"],
         ["Alexis@gmail.com", "Alexis"],
         ["Reda@gmail.com", "Reda"],
         ["Pierre@gmail.com", "Pierre"],
         ["JBG@gmail.com", "JBG"],
         ["JB@gmail.com", "JB"],
         ["Nico@gmail.com", "Nico"],
         ["Kino@gmail.com", "Kino"],
         ["Cécile@gmail.com", "Cécile"],
         ["Sabrina@gmail.com", "Sabrina"],
         ["Peggy@gmail.com", "Peggy"],
         ["Andy@gmail.com", "Andy"],
         ["Math@gmail.com", "Math"],
         ["Seb@gmail.com", "Seb"],
         ["Alexandre@gmail.com", "Alexandre"],
         ["Vincent@gmail.com", "Vincent"],
         ["Emilie@gmail.com", "Emilie"]]


def _test_hist_repas(date_debut, id_utilisateur,
                     nbr_date=365,
                     nbr_ingredients=4):
    """Fonction qui génère des dates de repas ainsi que leurs contenus en
    aliments sur une période définie en paramètre et renvoie cette historique
    sous forme d'une dataframe multi_indexé ou chaque repas sert d'indexe 1
    et chaque ingrédient est l'indexe 2
    """

    def generateur_repas(date, nbr_ingredients):
        """ Génère un repas sous forme de x ingredients suivi d'une quantité
        dans la structure repas passé en paramètre.
        """
        aliments = [
            "carotte", "petit pois", "pomme de terre", "jambon", "semoule",
            "yaourt", "escalope", "haricots verts", "tomates", "salade",
            "lardons", "pâtes", "concombre", "maïs", "pizza", "banane",
            "pomme", "abricot", "quiche"]

        repas = []
        for i in range(0, nbr_ingredients):
            # choisi un aliment aléatoire
            aliment = np.random.choice(aliments)
            # une quantité entre 30 et 120 grammes
            quantite = int(np.random.randint(30, 120, 1))
            repas += [{"date": date, "nom": aliment, "quantite": quantite}]

        repas = pd.DataFrame(repas)
        return repas

    # Crée une liste qui servira de contenur a l'histoirque de repas
    historique_de_repas = []

    # A partir de la date de départ génère une date par jour
    dates = [date_debut]
    for i in range(nbr_date):
        dates += [dates[-1] + date.timedelta(days=1)]

    # Pour chaque date génère un repas
    for date_repas in dates:
        json_repas = generateur_repas(date_repas, 4).to_json()

        # Ici la structure définie précédament prend forme
        historique_de_repas += [{"id_utilisateur": id_utilisateur,
                                 "date_repas": date_repas,
                                 "json_repas": json_repas}]

    return historique_de_repas
