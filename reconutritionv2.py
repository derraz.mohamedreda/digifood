# -*- coding: utf-8 -*-
"""
RECOMMANDATION ALIMENTAIRES A PARTIR DES VALEURS NUTRITIONNELLES
@author: Joseph, Léa, Lee Roy

A partir des contenus des repas renseignés par un utilisateur, et des conseils
des nutritionnistes, on se propose de donner des recommandations de menus pour
rester au maximum de sa forme. L'algorithme se sert des informations
renseignées par des personnes similaires susceptibles d'avoir des habitudes
alimentaires les meilleures, ainsi que des sites web donnant des conseils et
des recettes. Chaque utilisateur peut ainsi apprendre de lui-même et des
autres.

1 - LES DONNEES UTILISEES
La source de données la plus détaillée permettant d'accéder aux
caractéristiques des aliments disponibles sur le marché est Open Food Fact.
Le site donne accès à un fichier qui enjuin 2020 fournis 1,3 millions
d'aliments caractérisés par 180 champs. Schématiquement les informations
données sont

id , nom_produit   , proteine_100g,  glucide_100g, lipide_100g ... etc
1  , Céréal        , 20           ,  50          , 30
2
3
.
.
.

Les types d'informations accessibles sont les suivants:
    - l'identification du produit, lieu de fabrication...
    - compositions (glucide, lipide, protéine, minnéraux, vitamines...)

Des analyses nutritionelles seront menées

# TODO :

1°) Créer les fonction de calcul de base:
    1-a) (Joseph) valeur_nutritionnelle et bilan_nutritionnel
    1-b) (Lee Roy) carences

2°) Faire les graphiques d'analyse

"""

# Import

import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns

conseils_nutritionnels = {
    "valeur": {
        "glucides": 260,
        "fibres": 27,
        "lipides": 90},
}

valeur_nutritionnelle = {
    "carotte": {"glucides": 6.45, "fibres": 2.7, "lipides": 5.42},
    "pomme de terre": {"glucides": 16.7, "fibres": 1.8, "lipides": 0.86},
    "jambon": {"glucides": 1.3, "fibres": 0, "lipides": 1.3},
    "semoule": {"glucides": 72.83, "fibres": 3.9, "lipides": 0},
    "yaourt": {"glucides": 6.68, "fibres": 0, "lipides": 4.66},
    "escalope": {"glucides": 0, "fibres": 0, "lipides": 0},
    "haricots verts": {"glucides": 3.63, "fibres": 4, "lipides": 1},
    "tomates": {"glucides": 2.26, "fibres": 1.2, "lipides": 2.25},
    "salade": {"glucides": 1.37, "fibres": 1.3, "lipides": 1.7},
    "lardons": {"glucides": 0.6, "fibres": 0, "lipides": 0.46},
    "pâtes": {"glucides": 29.7, "fibres": 2.28, "lipides": 5.42},
    "petit pois": {"glucides": 4.7, "fibres": 5.8, "lipides": 0.47},
    "concombre": {"glucides": 2.04, "fibres": 0.6, "lipides": 1.34},
    "abricot": {"glucides": 7.14, "fibres": 1.8, "lipides": 6.57},
    "quiche": {"glucides": 22.2, "fibres": 0.90, "lipides": 1.7},
    "maïs": {"glucides": 15.41, "fibres": 1.7, "lipides": 4.54},
    "pizza": {"glucides": 27.7, "fibres": 2.36, "lipides": 2.6},
    "banane": {"glucides": 19.6, "fibres": 1.9, "lipides": 14.8},
    "pomme": {"glucides": 11.6, "fibres": 1.4, "lipides": 9.35}}


def valeur_repas(repas, valeur_aliments):
    """Fonction qui ressort la valeur nutritionel d'un repas sous forme de
    dictionaire.
    """
    apports_nutri = {"glucides": 0, "fibres": 0, "lipides": 0}

    for aliment, quantite in zip(repas["nom"], repas["quantite"]):
        nom = aliment
        if nom in valeur_aliments:
            for nutriment in valeur_aliments[nom]:
                val_gramme = valeur_aliments[nom][nutriment] / 100
                apports_nutri[nutriment] += (val_gramme * quantite)
    return apports_nutri


def bilan_nutritionnel(repas, valeur_nutri, date_debut, date_fin):
    """Fonction qui ressort la valeur nutritionnel total des repas entre
    deux dates.
    """
    for date_repas in repas["date"]:
        if date_repas >= date_debut and date_repas <= date_fin:
            bilan = {}
            bilan = valeur_repas(repas, valeur_nutri)
    return bilan


def valeur_nutrition_sur_nbJour(dico, nbjour):
    """Cette fonction prend en paramètre un dictionnaire de valeurs conseillées
    sur une journée et multiplie ces valeurs par le nombre de jours désirés.
    """
    df = pd.DataFrame(dico)
    df_nbjour = df.apply(lambda x: x * nbjour, axis=1)
    return df_nbjour


def nutriments(base_de_donnee):
    """Cette fonction me permet de recuperer dynamiquement les nutriments
    present dans ma base de donnée alimentation/recette
    """
    pass


# Leeroy
def carences(df_repas, date_debut, date_fin, profil=None):
    """Cette fonction calcul les carrences d'un individu en observant ce qu'il
    aurait du consommer et sa consomation en nutriment sur une période définie
    en paramètre
    Elle renvoie deux dataframe correspondante.
    """

    def fonctionApply(x, dico=valeur_nutritionnelle, nutri=""):
        """sous fonction qui permet de calculé la quantité de nutriment du
        de l'aliment basé sur sa quantité."""
        if x["nom"] in dico:
            return (dico[x["nom"]][nutri] * x["quantite"]) / 100

    """Affiche les carences selon l'alimentation sur x jour
    exemple profil : profil = conseils_nutritionnels["homme"]["20-30ans"]
    """
    df_sum = pd.Series()
    liste_nutriment = ["fibres", "glucides", "lipides"]
    for nutriment in valeur_nutritionnelle["carotte"]:
        df_repas[nutriment] = df_repas.apply(
            lambda x: fonctionApply(
                x, nutri=nutriment), axis=1)
    date_delta = date_fin - date_debut
    date_nbjour = date_delta.days
    df_conseil = valeur_nutrition_sur_nbJour(conseils_nutritionnels,
                                             date_nbjour)

    message = []
    for nutriment in liste_nutriment:
        df_sum[nutriment] = df_repas[nutriment].sum()
        if df_repas[nutriment].sum() < df_conseil.loc[nutriment, "valeur"]:
            message.append([f"vous avez une carence en : {nutriment}"])

    df_value = pd.DataFrame(df_sum, columns=["valeur"])
    return df_conseil, df_value


def difference_carence(personne, date_debut, date_fin):
    """A partir des deux dataframes obtenu par la fonction carence produit
    la valeur numérique de ces carences/exces
    """
    df_conseil, df_mes_repas = carences(personne.df_repas, date_debut,
                                        date_fin)

    df_diff = pd.DataFrame()
    df_diff["valeur"] = df_conseil["valeur"] - df_mes_repas["valeur"]

    return df_diff.round(decimals=2)


def suggestion_aliments(df_carences, valeur_nutritionnelle, tag_regime=[]):
    """"""
    # Pour chaque carences dans df_carences
    # observe valeur nutri pour chaque aliments, cherche les aliments
    # qui corresponde le mieux à la carrance
    
    ## Code Minimal
    # Observe les carrences
    # Querry aliments pour obtenir les 100 premier dans un order_by pour 
    # chaque carence
        # Elimine les aliments en fonction des tag_regime si possible/nécésaire
    
    ## Code Idéal : 
    # J'irai même à dire qu'il faudrais que chaque jour la bdd sauvegarde sur
    # une table les 100 meilleur de chaque nutriments
    # Querry aliments pour obtenir les 100 premier dans un order_by pour 
    # chaque carence
        # Elimine les aliments en fonction des tag_regime si possible/nécésaire
    
    # Prend en priorité les aliments qui corresponde le mieux a plusieurs 
    # carence a la fois
    # Dans chaque liste d'aliments correspondant à une carrence,
    # trie en focntion d'une autre carrence, ainsi de suite. 


    liste_aliments = ["""contient le nom des aliments qui compense les
                      carrences qui compense la df_carence"""]
    return liste_aliments
    pass



# groupe
def rendu_graphique(personne, mode, date_debut=None, date_fin=None,
                    date_repas=None, fichier="graph_repas.png"):
    """Fonction de rendu graphique, cette fonction a pour but de produire
    un groupe de graphiques représentant une information sensée liée à
    la consommation alimentaire de l'utilisateur il prend comme paramètre une
    personne, un mode, et une ou plusieurs dates en fonction du mode choisi.

    Chaque mode produit un graph seaborn, référez vous à la documentation
    de seaborn.

    mode == "repas" : ce mode demande une seule date et fourni le bilan
    nutrionnel du repas lié à cette date, il renvoie deux graphiques un pour
    le repas cumulé et un autre pour les aliments.

    mode == "bilan" :  ce mode demande une date de début et une date de fin,
    il permet de visualiser la consommation en nutriments à travers le temps
    permettant d'observer des carences ou excès sur une durée définie.

    mode == "carence" : ce mode demande une date de début et de fin, il permet
    de visualiser les carences en nutriments en fonction de la personne cette
    visualisation se fait à travers un graph bar superposant la consommation
    sur les carences.

            :param personne : personne qui possède un historique de repas
            :param
            :param date_debut : date de début de l'observation
            :param date_fin : date de fin de l'observation
    """
    if mode == "repas":  # Léa
        """Graph Bar des nutriments cumulés d'un repas passé en paramètre
        avec MATPLOTLIB

        étapes pour afficher un graph mode "repas" :
            charger un utilisateur
            charger un repas
            appeler la fonction rendu_graphique
        """
        mask = (personne.df_repas["date"] == date_repas)
        # personne.df_repas[mask]
        val_repas = valeur_repas(personne.df_repas[mask],
                                 valeur_nutritionnelle)
        print(val_repas)
        repas = pd.DataFrame.from_dict(val_repas, orient='index')
        legende = list(repas.index)
        x = np.arange(len(repas))
        # récupération valeurs nutri pour l'axe des y
        y = []
        for valeur in repas.values:
            y += list(valeur)

        fig, ax = plt.subplots()
        plt.bar(x, y)
        plt.xticks(x, labels=legende)
        plt.title('Valeurs nutritionnelles')
        plt.xlabel('Nutriments')
        plt.ylabel('Quantité en grammes')
        plt.savefig(fichier, bbox_inches='tight')
        plt.show()

        """Graph bar (subplots) des nutriments par aliment d'un repas passé
        en paramètre avec SEABORN
        """
        mask = (personne.df_repas["date"] == date_repas)
        repas = personne.df_repas[mask]
        # reprise valeur_repas pour calculer et récupérer les apports
        # nutritionnels d'un repas en séparant ses aliments
        valeur_nutri = {}
        for aliment, quantite in zip(repas["nom"], repas["quantite"]):
            nom = aliment
            if nom in valeur_nutritionnelle:
                apports_nutri = {"glucides": 0, "fibres": 0, "lipides": 0}
                for nutriment in valeur_nutritionnelle[nom]:
                    val_gramme = valeur_nutritionnelle[nom][nutriment] / 100
                    apports_nutri[nutriment] += (val_gramme * quantite)
                    valeur_nutri[nom] = apports_nutri

        # transfert dico vers dataframe
        data = pd.DataFrame.from_dict(valeur_nutri, orient='index')
        # axes du graphique générés dynamiquement pour les subplots
        axes = {}
        i = 1
        for aliment in valeur_nutri:
            axes[aliment] = "ax" + str(i)
            i += 1
        axs = list(axes.values())

        sns.set(style="white", context="talk")
        f, axs = plt.subplots(len(axs), 1, figsize=(10, 10), sharex=True)
        # tracé du graphique
        i = 0
        for element in data.index:
            x = data.columns
            y = data.iloc[i]
            sns.barplot(x=x, y=y, palette="rocket", ax=axs[i])
            i += 1

        plt.title('Valeurs nutritionnelles par aliment')
        plt.savefig(fichier)
        plt.show()

    if mode == "bilan":  # Joseph //fait 12/06
        """Graph qui montre la consomation de chaque nutriments par jour
        pour la durée passée en paramètre
        https://seaborn.pydata.org/examples/wide_data_lineplot.html
        """

        def graph_bilan(repas, date_debut, date_fin):
            """fonction qui crée un graph des nutriments par jours entre
            deux date, une structure intermédiaire est crée sous forme
            de dataframe avec pour index les dates et pour colonnes nutriments
            """

            mask = ((personne.df_repas["date"] >= date_debut)
                    & (personne.df_repas["date"] <= date_fin))
            personne.df_repas[mask]
            dates = list(personne.df_repas[mask]["date"].value_counts().index)
            dico_val_repas = {}
            for date_repas in dates:
                filtre = personne.df_repas[personne.df_repas["date"]
                                           == date_repas]
                valeur = valeur_repas(filtre, valeur_nutritionnelle)
                dico_val_repas[date_repas] = valeur

            df = pd.DataFrame(dico_val_repas.values(),
                              index=dico_val_repas.keys())
            graph = sns.lineplot(data=df, palette="tab10", linewidth=2.5)
            graph.tick_params(axis='x', labelrotation=20)
            plt.title('Bilan nutritionnel')
            plt.savefig(fichier, bbox_inches='tight')

        graph_bilan(personne.df_repas, date_debut, date_fin)

    # Debut 12/06
    if mode == "carence":  # Lee-Roy
        """Graph double bar surperposition d'un nutriment sur son taux espéré
        avec carence
        https://seaborn.pydata.org/examples/horizontal_barplot.html
        """
        df_conseil, df_mes_repas = carences(personne.df_repas, date_debut,
                                            date_fin)

        sns.set(style="whitegrid")
        f, ax = plt.subplots(figsize=(6, 15))
        x1 = list(df_conseil.index)
        y1 = list(df_conseil["valeur"])
        sns.set_color_codes("pastel")
        f = sns.barplot(x=x1, y=y1, data=df_conseil,
                        label="conseillée", color="b")
        f.set(yscale="log")
        f.set_xticklabels(f.get_xticklabels(), rotation=90)
        sns.set_color_codes("muted")
        x2 = list(df_mes_repas.index)
        y2 = list(df_mes_repas["valeur"])
        ax = sns.barplot(x=x2, y=y2, data=df_mes_repas,
                         label="actuel", color="b")
        ax.set_xticklabels(ax.get_xticklabels(), rotation=90)
        ax.legend(ncol=2, loc="upper right", frameon=True)
        ax.set(xlabel="nutriments", ylabel="valeurs")
        sns.despine(left=True, bottom=True)
        plt.title('Carence nutrionnelle')
        plt.savefig(fichier, bbox_inches='tight')

    # Fin
